import domainparse
from html.parser import HTMLParser
import sys
import re
import dictionary
import similarity

'''
- Name starts with capital letter or number
- Ends after no capitals are found or an ending
- Points for containing fake words
- Points for being earlier in the document
- Points for being in title
'''
class NameChooser:
    
    # (name, position, in title, found endword, occur)
    
    def __init__(self, bias):
        self.nameList = []
        self.bias = bias
        print("bias "+self.bias)
        
    def isCapitalized(self, word):
        if word[0].isupper():
            return True
        return False
           
    def flagInside(self, word): 
    
        if self.isCapitalized(word):
            return True
        
        if word[0].isdigit():
            return True
        
        if word[0] == "&":
            return True
            
        if word == "and":
            return True
            
        if similarity.partial(word, self.bias):
            return True
        
        return False
            
    def flagEnd(self, word):  
    
        lword = word.lower()
        
        # business enders
        if len(lword) <= 4:
            if lword.find("inc") != -1:
                return True
                
            if lword.find("llc") != -1:
                return True
               
            if lword.find("co") != -1:
                return True

            if lword.find("p.c.") != -1:
                return True       

            if lword.find("llp") != -1:
                return True                       
           
        return False
        
    def cleanupResult(self, list):
        try:
            list.remove("Contact")
        except:
            list
        str = " ".join(list)
        str = re.sub(r"\\", "", str)
        return str
        
    def numberRatio(self, str):
        cnt = 0
        for c in str:
            if c.isnumeric():
                cnt += 1
        return float(cnt)/float(len(str))
        
    def isUndeterminable(self, word):
        if re.search("[^\w]", word) != None: # Has funny characters
            return True
        cnt = 0
        for c in word:
            if c.isupper():
                cnt += 1
                
        if cnt != len(word) and cnt > 1:
            return True
        return False
        
        
    def getResult(self):
        
        self.nameList.sort(key=lambda tup: tup[0])
        
        for i in range(0, len(self.nameList)-1):
            if self.nameList[i][0] == self.nameList[i+1][0]:
                tup1 = self.nameList[i]
                tup2 = self.nameList[i+1]
                self.nameList[i+1] = (tup1[0], int((tup1[1]+tup2[1])/2), tup1[2] or tup2[2], tup1[3] or tup2[3], tup1[4]+tup2[4])
                self.nameList[i] = None
                
        
        bestFit = ([], 0, 0)
        for tuple in self.nameList:
            if tuple == None:
                continue
                
            # Calculate points
            points = 0
            
            pointList = []
            
            if tuple[3]: # Points for having end word
                points += 5
                pointList.append("end word points")
                
            if tuple[2]: # Points for being in title
                #print(tuple[1])
                points += 4
                pointList.append("title points")
                
            if tuple[4] > 1:
                points += 4
                pointList.append("occurance points")
                
            hasDictPoints = False
            similarityMatches = 0
            for word in tuple[0]:
                #pointList.append("sim score: "+str(similarity.strings(self.bias, word)))
                if self.numberRatio(word) < 0.8:
                    if not dictionary.match(word) and not self.isUndeterminable(word):
                        if not hasDictPoints:
                            points += 4
                            pointList.append("non dict points")
                            hasDictPoints = True
                    elif dictionary.matchState(word):
                        points -= 6
                        pointList.append("state points")
                if similarity.partial(word, self.bias):
                    similarityMatches +=1
                    
            similarityMetric = float(similarityMatches) / float(len(tuple[0]))
            if similarityMetric == 1.0:
                points += 6
                pointList.append("similarity points T1")
            elif similarityMetric > 0.5:
                points += 5
                pointList.append("similarity points T2")
            elif similarityMetric > 0.1:
                points += 4    
                pointList.append("similarity points T3")
                
            if bestFit[1]/2 < points:
                print(pointList)
                print(str(points)+" "+str(tuple))
                print()
            
            if points > bestFit[1] or (points == bestFit[1] and tuple[1] < bestFit[2]):
                bestFit = (tuple[0], points, tuple[1])
                
                
        return self.cleanupResult(bestFit[0])
    
    def appropriateTag(self, tag):
        if tag == "title" or tag == "p" or tag == "span":
            return True
        return False
        
    def appropriateAttr(self, attr):
        if attr == "title" or attr == "alt":
            return True
        return False
        
        
    def parseData(self, tag, attr, pos, data):
    
        if not self.appropriateTag(tag) and not self.appropriateAttr(attr):
            return
    
        # Simple appoximate beggining and end of possible names
        words = data.split(" ")
        inTitle = False
        if tag == "title":
            inTitle = True
        potentialWordList = []
        for word in words:
            if len(word) == 0:
                continue          
            inWord = self.flagInside(word)
            endWord = self.flagEnd(word)
            if inWord:
                word = re.sub(r"\\", "", word)
                potentialWordList.append(word)
            if inWord and endWord:
                self.nameList.append((potentialWordList, pos, inTitle, True, 1))
                potentialWordList = []                
            if not inWord and not endWord:
                if potentialWordList:
                    self.nameList.append((potentialWordList, pos, inTitle, False, 1))
                    potentialWordList = []
        if potentialWordList:
            self.nameList.append((potentialWordList, pos, inTitle, False, 1))
                    
        
                
class GetWebsiteInfo(HTMLParser):
    
    curTag = ""
    curAttr = ""
    curData = ""
    
    # Init
    
    def __init__(self, url):
        HTMLParser.__init__(self)
        self.url = url
        print("url: "+url)
        print("domain: "+domainparse.get_domain(url))
        self.nameChooser = NameChooser(domainparse.get_domain(url))
        visited = domainparse.get_domain_data(url,self.parsecb, 15)
        print("Best fit title: "+self.nameChooser.getResult())
        print("Parsed "+str(len(visited))+" files")
        
    def parsecb(self,url,data):
        print("("+url+")")
        self.feed(data)        
       
        
    # Overrides
    
    def handle_starttag(self, tag, attrs):
        self.curTag = tag
        for attr in attrs:
            self.curAttr = attr[0]
            if attr[1] is not None:
                self.processData(attr[1])
        
    def handle_startendtag(self, tag, attrs):
        self.curTag = tag
        for attr in attrs:
            self.curAttr = attr[0]
            if attr[1] is not None:
                self.processData(attr[1])
        
    def handle_data(self, data):
        self.curAttr = ""
        self.processData(data)
        
    # Processing
    
    def docull(self, data):
        useless = ["\\n","\\t"," ","'"]
        cnt = 0
        for s in useless:
            cnt += self.countOccurances(data, s)
        threshold = len(data)/2
        #print("threshold "+str(cnt)+" of "+str(threshold))
        if cnt < threshold:
            return False
        return True
        
    def processData(self, data):    
        if self.docull(data):
            return
        pos = self.getpos()[1]
        #print("Tag("+self.curTag+") Attr("+self.curAttr+") Data("+data+")")
        self.nameChooser.parseData(self.curTag, self.curAttr, pos, data)

    # Util
    
    def findPossibleCompanyNames():
        print(1)
        
    def countOccurances(self, str1, str2):
        o = 0
        for i in range(0,len(str1)):
            n = i+len(str2)
            if str1[i:n] == str2:
                o += 1
        return o
    
        

info = GetWebsiteInfo(sys.argv[1])
