import re
import json

def cindex(str):
    str = str.strip().lower()
    return re.sub("[-\\.]", "", str)[:2]

def match(str):
    str = str.lower()
    index = json.loads(open("dictionary-index.txt").read())
    f = open("dictionary.txt")
    entry = None
    try:
        entry = index[cindex(str)]
    except:
        return False
    f.seek(entry[0],0)
    for i in range(0, entry[1]):
        cstr = f.readline().strip().lower()
        if str == cstr:
            return True
    return False
    
def matchState(str):
    str = str.lower()
    f = open("states.txt")
    for line in f:
        line = line.strip().lower()
        if len(line) == 2:
            if line == str:
                return True
        elif str.find(line) != -1:
            if float(len(line)) / float(len(str)) >= 0.5:
                return True
    return False
    
    