import re
import json

def cindex(str):
    str = str.strip().lower()
    return re.sub("[-\\.']", "", str)[:2]

f = open("dictionary.txt", "r")

indexDict = dict()

firstLine = f.readline().strip()
curStr = cindex(firstLine)
startIndex = 0
lines = 1
filePos = f.tell()
while True:
    line = f.readline().strip()
    print("("+line+")")
    if line == "":
        break
    if curStr == cindex(line):
        lines += 1
    else:
        indexDict[curStr] = (startIndex, lines)
        startIndex = filePos
        lines = 1
        curStr = cindex(line)
    filePos = f.tell()
        
indexDict[curStr] = (startIndex, lines)
f.close()

fw = open("dictionary-index.txt", "w")

fw.write(json.dumps(indexDict))

fw.close()

#test
#print(indexDict)

'''
f = open("dict.txt", "r")
fw = open("res.txt", "w")
for entry in indexDict.items():
    f.seek(entry[1][0],0)
    fw.write("Index: "+entry[0]+"\n")
    for i in range(0, entry[1][1]):
        fw.write("("+f.readline().strip()+")"+"\n")
f.close()
'''
