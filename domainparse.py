
import sys
import urllib.request
import re
from urllib.parse import urlparse
from urllib.request import Request, urlopen
from urllib.error import URLError, HTTPError
import traceback

def plist(list):
    for l in list:
        print(l)

def get_domain(url):
    o = urlparse(url)
    str = o.netloc
    if str == "":
        str = url
    return re.sub("www\\.", "", str)
    
def find_all_urls(data):
    return re.findall("(?:\\\"|\\\')((?:(?:http://)|(?:https://)|(?:www)|(?:'+name+')|(?:/))[\\w\\d=%&+_\\-/\\.]+)(?:\\\"|\\\')", data)
    
def in_domain(str, domain):
    if domain in str:
        return True
    if str[0] == '/':
        return True
    return False


def get_abs_url(str, domain=""):
    if domain != "" and str[0] == '/':
        str = "http://"+domain+str
    if str[0:4] != "http":
        str = "http://"+str
    str = re.sub("www\\.", "", str)
    if str[-1] == "/":
        str = str[:-1]
    return str

def should_cull(url):
    if re.search("\\.(jpg|jpeg|css|js|png|cgi|pdf|gif)$", url) == None:
        return False
    return True
    
def get_data(url):
    url = get_abs_url(url)
    try:
        return urllib.request.urlopen(url).read()
    except HTTPError as e:
        print("Error parsing url "+url+" ("+str(e.code)+")")
    except URLError as e:
        print("Error parsing url "+url+" ("+str(e.reason)+")")        
    except:
        print("Error parsing url "+url)
        traceback.print_exc()
    return ""
    
def get_domain_data(url, func, depth=0):
    url = get_abs_url(url)
    domain = get_domain(url)

    # Traverse time
    tovisit = [url]
    visited = []
    iter = 0
    while tovisit:
        if depth!=0 and iter>=depth:
            break
        curUrl = tovisit.pop()
        if curUrl in visited:
            continue
        visited.append(curUrl)
        try:
            rawWebData = str(urllib.request.urlopen(curUrl).read())
            func(curUrl, rawWebData)
            urlsFound = findAllUrls(rawWebData)
            for u in urlsFound:
                u = getAbsUrl(u, domain)
                if u not in visited and inDomain(u, domain) and not shouldCull(u):
                    tovisit.append(u)
        except HTTPError as e:
            print("Error parsing url "+curUrl+" ("+str(e.code)+")")
        except URLError as e:
            print("Error parsing url "+curUrl+" ("+str(e.reason)+")")        
        except:
            print("Error parsing url "+curUrl)
            traceback.print_exc()
        iter += 1
    return visited




